/**
 * Javascript for extension
 * @ingroup Extensions
 * @author Josef Martiňák
 */

( function ( mw, $ ) {

	var wikipath = window.location.origin;
	// Send modal form
	$('#RCsettings').submit(function( event ) {
		event.preventDefault();

		// send feedback
		var addurl = $(this).find('#addurl').prop('checked') ? 'true': 'false';
		var edit = $(this).find('#edit').prop('checked') ? 'true': 'false';
		var countryFilter = $(this).find('#countryFilter').prop('checked') ? 'true': 'false';
		var createaccount = $(this).find('#createaccount').prop('checked') ? 'true': 'false';
		var create = $(this).find('#create').prop('checked') ? 'true': 'false';
		var badlogin = $(this).find('#badlogin').prop('checked') ? 'true': 'false';

		var data = {'addurl': addurl, 'edit': edit, 'countryFilter': countryFilter, 'create': create, 'createaccount': createaccount, 'badlogin': badlogin};
		$.ajax({
			type: 'POST',
			url: wikipath + '/index.php?title=Special:CaptchaSettings',
			data: data,
			dataType: 'text',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			success: function( server_response ) {}
		});
	});

}( mediaWiki,jQuery));