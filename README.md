# CaptchaSettings

Mediawiki extension.

## Description

* _CaptchaSettings_ allows setting the Captcha behaviour.
* Version 1.1

## Installation

* Make sure you have MediaWiki 1.29+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'CaptchaSettings' )`;
* Add file with name _$wgSitename.php_ (see LocalSettings.php) to the extension's folder.
* In case of wikifarm more files like this may be added.
* Include this to your _LocalSettings.php_: `require_once("$IP/extensions/CaptchaSettings/config/$wgSitename.php");`
* Set access in _LocalSettings.php_:

```php
$wgGroupPermissions['*']['captchasettings'] = false;
$wgGroupPermissions['user']['captchasettings'] = false;
$wgGroupPermissions['sysop']['captchasettings'] = false;
$wgGroupPermissions['bureaucrat']['captchasettings'] = true;
```

* Allow write access to _config/$wgSitename.php_ files.

## SpecialPage

Special:CaptchaSettings

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Release Notes

### 1.1

Caching issues fix. Using JS.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University
