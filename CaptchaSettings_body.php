<?php

/**
 * SpecialPage file for CaptchaSettings
 * Allows setting Captcha behaviour
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class CaptchaSettings extends SpecialPage {
	function __construct() {
		parent::__construct( 'CaptchaSettings', 'editinterface' );
	}
 
	function execute($param) {
		global $wgSitename, $wgServer;
		
		$this->checkPermissions();
		$this->setHeaders();
		$request = $this->getRequest();
		$out = $this->getOutput();
		$out->enableClientCache(false);
		$user = $this->getUser();

		// zjisti path k souboru s nastavenim
		$settings = __DIR__ . "/config/$wgSitename.php";

		// zapis zmeny
		if($request->wasPosted()){
		
			$data = "<?php\n\n";
			
			$data .= "\$wgCaptchaTriggers['addurl'] = ";
			if($request->getText("addurl") == 'true') {
				$data .= "true";
			}
			else {
				$data .= "false";
			}
			$data .= ";\n\$wgCaptchaTriggers['edit'] = ";
			if($request->getText("edit") == 'true') {
				$data .= "true";
			}
			else {
				$data .= "false";
			}
			$data .= ";\n\$wgCaptchaTriggers['create'] = ";
			if($request->getText("create") == 'true') {
				$data .= "true";
			}
			else {
				$data .= "false";
			}
			$data .= ";\n\$wgCaptchaTriggers['createaccount'] = ";
			if($request->getText("createaccount") == 'true') {
				$data .= "true";
			}
			else {
				$data .= "false";
			}
			$data .= ";\n\$wgCaptchaTriggers['badlogin'] = ";
			if($request->getText("badlogin") == 'true') {
				$data .= "true";
			}
			else {
				$data .= "false";
			}
			$data .= ";\n\$countryFilter = ";
			if($request->getText("countryFilter") == 'true') {
				$data .= "true";
			}
			else {
				$data .= "false";
			}
			$data .= ";\n\n?>";

			$out->disable();
			header( 'Content-type: application/text; charset=utf-8' );

			if(!file_put_contents($settings,$data)) {
				echo "permission-error";
			}
			echo "ok";
			return true;
		}

		
		# nacti aktualni nastaveni
		require($settings);
		
		$addurl = $edit = $create = $createaccount = $badlogin = $cf = "";
		if($wgCaptchaTriggers['addurl']) {
			$addurl = "checked='checked'";
		}
		if($wgCaptchaTriggers['edit']) {
			$edit = "checked='checked'";
		}
		if($wgCaptchaTriggers['create']) {
			$create = "checked='checked'";
		}
		if($wgCaptchaTriggers['createaccount']) {
			$createaccount = "checked='checked'";
		}
		if($wgCaptchaTriggers['badlogin']) {
			$badlogin = "checked='checked'";
		}
		if($countryFilter) {
			$cf = "checked='checked'";
		}
		
		$out->addModules('ext.CaptchaSettings');
		
		# zobraz nastaveni
		$output = "<br/>".$this->msg('captchasettings-desc')->text()."<br/><br/>";	
		$output .= "<form id='RCsettings' name='RCsettings' method='post' action=''>\n";
		$output .= "<input type='checkbox' name='addurl' id='addurl' value='1' $addurl />\n";
		$output .= "<label for='addurl'>addurl (".$this->msg('captchasettings-addurl')->text().")</label><br/>\n";
		$output .= "<input type='checkbox' name='edit' id='edit' value='1' $edit />\n";
		$output .= "<label for='edit'>edit (".$this->msg('captchasettings-edit')->text().")</label><br/>\n";
		$output .= "<input type='checkbox' name='countryFilter' id='countryFilter' value='1' $cf />\n";
		$output .= "<label for='countryFilter'>".$this->msg('captchasettings-countryFilter')->text()."</label><br/>\n";
		$output .= "<input type='checkbox' name='create' id='create' value='1' $create />\n";
		$output .= "<label for='create'>create (".$this->msg('captchasettings-create')->text().")</label><br/>\n";
		$output .= "<input type='checkbox' name='createaccount' id='createaccount' value='1' $createaccount />\n";
		$output .= "<label for='createaccount'>createaccount (".$this->msg('captchasettings-createaccount')->text().")</label><br/>\n";
		$output .= "<input type='checkbox' name='badlogin' id='badlogin' value='1' $badlogin />\n";
		$output .= "<label for='badlogin'>badlogin (".$this->msg('captchasettings-badlogin')->text().")</label><br/><br/>\n";
		$output .= "<input type='submit'/>\n";
		$output .= "</form>\n";
		$out->addHTML($output);
	}
}